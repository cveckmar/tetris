var dropTimer = 0;
var  lastTime = 0;
var actual_JSON;
var canvas;


//loading json from file by sending request
function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', 'package.json', true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

//initialision of canvas and parsing json text
function init() {
    loadJSON(function(response) {
        actual_JSON = JSON.parse(response);
    });


    canvas = document.getElementById("tetris");
    canvas.getContext('2d').scale(20, 20);
}

//click on pause(button)
function pauseG(){
    if (lastTime < 10000)
        lastTime = 20000;
    else
        lastTime*=2;
}

//click on continue(button)
function continueG(){
    lastTime = 0;
}

var audio = new Audio('sound.mp3');

//click on start(button)
function startG() {
    var area;
    let game_s;
    let colors;
    let update;
    let pieceRotate;
    let pieceMove;
    let pieceDrop;
    let updateScoreLevel;
    let newPiece;
    let join;
    let draw;
    let drawMatrix;
    let deleteRows;
    let changeLevel;
    let canvas = document.getElementById("tetris"), ctx = canvas.getContext('2d');

    changeLevel = () => {
        if (game_s.score > 1100) {
            game_s.level = 10;
        } else if (game_s.score > 900) {
            game_s.level = 9;
        } else if (game_s.score > 780) {
            game_s.level = 8;
        } else if (game_s.score > 650) {
            game_s.level = 7;
        } else if (game_s.score > 530) {
            game_s.level = 6;
        } else if (game_s.score > 450) {
            game_s.level = 5;
        } else if (game_s.score > 360) {
            game_s.level = 4;
        } else if (game_s.score > 180) {
            game_s.level = 3;
        } else if (game_s.score > 100) {
            game_s.level = 2;
        }
    };

    //full row is emptied, score(level) is added
    deleteRows = () => {
        let row_n = 1;

        outer: for (let y = 0; y < area.length; y++) {
            for (let x = 0; x < area[y].length; x++) {
                if (area[y][x] !== 0) {
                } else continue outer;
            }
            const row = area.splice(y, 1)[0].fill(0);
            area.unshift(row);
            y++;

            game_s.score += row_n * 10;
            changeLevel();
            row_n++;
        }
    };

    //add color to canvas by selected pieces
    drawMatrix = (matrix, num) => {
        matrix.forEach((row, y) => {
            row.forEach((value, x) => {
                if (value !== 0) {
                    ctx.fillStyle = colors[value];
                    ctx.fillRect(x + num.x, y + num.y, 1, 1);
                }
            });
        });
    };

    //coloring whole tetris area
    draw = () => {
        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        drawMatrix(area, {x: 0, y: 0});
        drawMatrix(game_s.piece, game_s.point);
        ctx.beginPath();
        ctx.strokeStyle = 'black';
        ctx.lineWidth = 0.1;

        for (i = 1; i < 20; i++) {
            ctx.moveTo(0, i);
            ctx.lineTo(canvas.width, i);
            ctx.stroke();
            ctx.moveTo(i, 0);
            ctx.lineTo(i, canvas.width / 2);
            ctx.stroke();
        }
    };

    //join to dropped pieces  or ground
    join = (area, piece) => {
        piece.piece.forEach((row, y) => {
            row.forEach((value, x) => {
                if (value !== 0) {
                    area[y + piece.point.y][x + piece.point.x] = value;
                }
            });
        });
    };

    //create new piece of tetris models
    newPiece = () => {
        const pieces = 'IJLOSTZ';
        game_s.piece = createPiece(pieces[Math.floor(Math.random() * pieces.length)]);
        game_s.point.y = 0;
        game_s.point.x = (Math.floor(area[0].length / 2)) -
            (Math.floor(game_s.piece[0].length / 2));
        if (validDrop(area, game_s.piece, game_s.point)) {
            game_s.exit = 1;
        }
    };

    updateScoreLevel = () => {
        document.getElementById('score').innerText = game_s.score;
        document.getElementById('level').innerText = game_s.level;
    };

    //falling tetris pieces and color them
    pieceDrop = () => {
        game_s.point.y++;
        if (validDrop(area, game_s.piece, game_s.point)) {
            game_s.point.y--;
            join(area, game_s);
            newPiece();
            deleteRows();
            updateScoreLevel();
        }
        dropTimer = 0;
    };

    // moving piece to sides
    pieceMove = num => {
        game_s.point.x += num;
        if (validDrop(area, game_s.piece, game_s.point)) {
            game_s.point.x -= num;
        }
    };

    // rotating piece controlling touches with wall
    pieceRotate = () => {
        const pos = game_s.point.x;
        let num = 1;
        rotate(game_s.piece);
        while (validDrop(area, game_s.piece, game_s.point)) {
            game_s.point.x += num;
            if (num > 0) {
                num = -(num + 1);
            } else {
                num = -(num + -1);
            }
            if (num > game_s.piece[0].length) {
                rotate(game_s.piece);
                game_s.point.x = pos;
                return;
            }
        }
    };

    //real time computing
    update = (time = 0) => {
        const deltaTime = time - lastTime;

        dropTimer += deltaTime;
        if (dropTimer > (1000 / game_s.level)) {
            pieceDrop();
        }
        if (game_s.exit === 1) {
            audio.play();
            return;
        }
        lastTime = time;
        draw();
        requestAnimationFrame(update);
    };

    //controlling og pieces
    document.addEventListener('keydown', event => {
        switch (event.key) {
            case "Down": // IE/Edge specific value
            case "ArrowDown":
                pieceDrop();
                break;
            case "Up":
            case "ArrowUp":
                pieceRotate();
                break;
            case "Left":
            case "ArrowLeft":
                pieceMove(-1);
                break;
            case "Right":
            case "ArrowRight":
                pieceMove(1);
        }
    });

    colors = ['black', 'purple', 'yellow', 'orange', 'blue', 'aqua', 'green', 'red'];


    //structure which define state of piece, type of piece..
    game_s = {
        point: {x: 0, y: 0},
        piece: null,
        score: 0,
        level: 1,
        exit: 0,
    };

    area = createMatrix4piece(12, 20);

 newPiece();
 updateScoreLevel();
 update();

}


function createPiece(type) {
    if (type === 'I') {
        return  actual_JSON.I;
    } else if (type === 'J') {
        return actual_JSON.J;
    } else if (type === 'L') {
        return actual_JSON.L;
    } else if (type === 'O') {
        return actual_JSON.O;
    } else if (type === 'S') {
        return actual_JSON.S;
    } else if (type === 'T') {
        return actual_JSON.T;
    } else if (type === 'Z') {
        return actual_JSON.Z;
    }
}











function rotate(piece) {
    piece.reverse();
    for (let y = 0; y < piece.length; ++y) {
        for (let x = 0; x < y; ++x) {
            [
                piece[x][y],
                piece[y][x],
            ] = [
                piece[y][x],
                piece[x][y],
            ];
        }
    }
}

function createMatrix4piece(w, h) {
    const matrix = [];
    for (h;h>0;h--) {
        matrix.push(new Array(w).fill(0));
    }
    return matrix;
}

//controlling of touching ground
function validDrop(area, piece, pos) {
    for (let y = 0; y < piece.length; y++) {
        for (let x = 0; x < piece[y].length; ++x) {
            if (piece[y][x] !== 0 &&
                (area[y + pos.y] &&
                    area[y + pos.y][x + pos.x]) !== 0) {
                return true;

            }
        }
    }
    return false;
}
